\begin{definition}
	Eine \textbf{Gruppe} ist ein Paar \((G,\cdot)\), wobei \(G\) eine Menge, \(\cdot\) eine Verknüpfung auf \(G\) sind, wobei gilt
	\begin{enumerate}[(i)]
		\item Assoziativgesetz: \(a \cdot (b \cdot c) = (a \cdot b) \cdot c \quad \forall a, b, c \in G \)
		\item Neutralelement: \(\exists e \in G : a \cdot e = e \cdot a = a \quad \forall a \in G\)
		\item Inverse: Für jedes \(a \in G\) gibt es ein \(b \in G\) mit \(a \cdot b = e \) und \(b\cdot a = e\).
	\end{enumerate}

	Gilt zusätzlich \(a\cdot b = b \cdot a \  \forall a,b \in G\), dann heißt \(G\) \textbf{abelsch} oder \textbf{kommutativ}.
\end{definition}

Bei einem \enquote{punktartigen} Verknüpfungssymbol (\(\cdot, \odot, \ast\)) verwendet man \textbf{multiplikative}, bei \enquote{plusartigem} Symbol \textbf{additive} Schreibweise für die Gruppe. Außerdem verwendet man folgende Schreibweisen:
\begin{itemize}
	\item Neutralelement: (häufig) \(1_G\) bei multiplikativer, \(0_G\) bei additiver Schreibweise.
	\item Inverse: \(g^{-1}\) bei multiplikativer, \(-g\) bei additiver Schreibweise.
	\item Potenzen: \(g^n\) bei multiplikativer, \(n  g\) bei additiver Schreibweise. \\
	Ist \(n \in \N\), dann gilt \(g^n = \underbrace{g\cdot\ldots \cdot g}_{n\text{-mal}}\) bei multiplikativer und \(n g = \underbrace{g + \ldots + g}_{n\text{-mal}}\) bei additiver Schreibweise.
\end{itemize}

\begin{definition}
	Eine Abbildung \(\phi: G \to H\) zwischen Gruppen \((G,\cdot), (H, \ast)\) heißt \textbf{Gruppenhomomorphismus}, wenn gilt:
	\[\phi(g_1 \cdot g_2) = \phi(g_1)\ast\phi(g_2) \ \forall g_1, g_2 \in G\]
	(Erinnerung: Es gilt dann immer \(\phi(e_G) = e_H\), \(\phi(g^{-1}) = \phi(g)^{-1} \ \forall g \in G\))

	In additiver Schreibweise: \((G,+), (H,\oplus)\)
	\[\phi(g_1 + g_2) = \phi(g_1) \oplus \phi(g_2)\]
	\[ \phi(0_G) = 0_H, \quad \phi(-g) = -\phi(g)\]
\end{definition}

\begin{bsp}
	\(\exp: (\R,+) \to (\R^+, \cdot)\)
\end{bsp}

\begin{definition}
	Zwei Gruppen \(G, H\) heißen \textbf{isomorph}, wenn ein Isomorphismus, also ein bijektiver Hom. zwischen \(G\) und \(H\) existiert.
\end{definition}

Wichtig: Isomorphe Gruppen haben übereinstimmende algebraische Eigenschaften (\(|G|= |H|\), d.h. gleiche Ordnung, gleichviele Elemente der Ordnung \(d\) für beliebiges \(d \in \N\), gleichviele Untergruppen usw.)

\begin{definition}
	Sei \((G, \cdot)\) eine Gruppe, \(U \subseteq G\).

	\(U\) heißt \textbf{Untergruppe} von \(G\), genau dann wenn \(e_G \in U\) und für alle \(a,b \in U\) gilt \(ab \in U\) und \(a^{-1} \in U\)

	(\(\Rightarrow (U, \cdot_U)\) ist Gruppe, wobei \(\cdot_U: U \times U \to U\) durch Einschränkung von \(\cdot : G \times G \to G \) zustande kommt)
\end{definition}

\begin{bsp}[abelsche Gruppen]
	\begin{enumerate}[(i)]
		\item \((R,+)\) falls \(R\) Ring (oder Körper), z.B. \((\Z,+), (\Q, +), \ldots\)
		\item \((K^\times, \cdot)\) wobei \(K\) Körper, \(K^\times = K\backslash\{0_K\}\) z.B. \((\Q^\times, \cdot)\).

		Achtung: \((\Q, \cdot)\) ist keine Gruppe, auch \((\N_0, +), (\N, +), (\N, \cdot), (\Z, \cdot)\) nicht. Es fehlen für die meisten Elemente die Inversen!
		\item Allgemeiner: \((R^\times, \cdot)\) für einen Ring \(R\), wobei \(R^\times\) die \textbf{Einheitengruppe} von \(R\) bezeichnet, also \(R^\times = \{ a \in R \suchthat \exists b \in R: a \cdot b = 1_R\}\)

		Achtung: \(K^\times = K \backslash\{0_K\}\) gilt \underline{nur} für Körper!

		Wichtiges Beispiel einer Einheitengruppe: prime Restklassengruppen \[(\nicefrac{\Z}{n\Z})^\times = \left\{\overline{a} \in \nicefrac{\Z}{n\Z} \mid \ggt(a,n) = 1\right\}\]
		Beispiel: \( (\nicefrac{\Z}{8\Z}) = \{\overline{0}, \overline{1}, \overline{2}, \ldots, \overline{7}\}\)
		\( \Rightarrow (\nicefrac{\Z}{8\Z})^\times = \{ \overline{1}, \overline{3}, \overline{5}, \overline{7}\}\)

		Die Ordnung von \((\nicefrac{\Z}{n\Z})^\times\) ist jeweils \(\varphi(n)\), wobei \(\varphi\) die Eulersche \(\varphi\)-Funktion bezeichnet.

	\item Sind \(G, H\) abelsche Gruppen, dann auch \(G\times H\). Bsp: \((\nicefrac{\Z}{2\Z} \times \nicefrac{Z}{2\Z}, +) \)
	\item \((V,+)\) falls V K-Vektorraum (K Körper) Bsp.: \((\R^n,+)\), \(\mathcal{M}_{m \times n}, +)\), wobei \(\mathcal{M}_{m \times n, K}\) Menge der \(m \times n\)- Matrizen über einem Körper K
	\end{enumerate}

\end{bsp}

\begin{bsp}[für nicht-abelsche Gruppen]
	\begin{enumerate}[(i)]
		\item symmetrische Gruppe \(S_n\) (nicht-abelsch für \(n \geq 3\)) \\
		alternierende Gruppe \(A_n\) (nicht-abelsch für \(n \geq 3\)) \\
		Diedergruppe \(D_n\) mit \(2n\) Elementen (nicht-abelsch für \(n \geq 3\))
		\item allgemeine lineare Gruppe \(\GL_n(k)\) (K Körper, \(n \geq 2\))
		\item semidirekte Produkte \(N \rtimes U\) (nicht-abelsch falls N nicht abelsch, U nicht abelsch oder der Homomorphismus \(\phi : U \to \Aut(N)\) nichttrivial ist, d.h. es gilt nicht \(\phi(u) = \id_N \forall u \in U\))
	\end{enumerate}
\end{bsp}

\begin{erinnerung}[Zentrum einer Gruppe]
	\[Z(G) = \left\{a \in G \suchthat a\cdot b = b \cdot a \ \forall b \in G\right\}\]
	klar: G abelsch \(\Leftrightarrow Z(G) = G\) 
\end{erinnerung}
Übung: Wenn G eine Gruppe und U eine abelsche Untergruppe von G ist, gilt dann \(U \subseteq Z(G)\) ? (Beweis oder konkretes Gegenbeispiel)

\za \aufg{F12T1A1}, \aufg{F13T2A2}

\begin{definition}
	Sei G eine endliche Gruppe und p eine Primzahl. Eine Untergruppe U von G heißt \textbf{p-Untergruppe}, falls \(|U| = p ^{m}\) für ein \(m \in N_0\) gilt, und \textbf{p-Sylowgruppe}, wenn \(m\) zusätzlich maximal ist, also \(p ^{m+1} \nmid |G|\) gilt.
\end{definition}

