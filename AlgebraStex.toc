\select@language {german}
\contentsline {section}{\numberline {1}Elementare Gruppentheorie}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Theorie}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Aufgaben}{10}{subsection.1.2}
\contentsline {section}{\numberline {2}Die symmetrischen Gruppen}{19}{section.2}
\contentsline {subsection}{\numberline {2.1}Theorie}{19}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Aufgaben}{21}{subsection.2.2}
\contentsline {section}{\numberline {3}Fortgeschrittene Gruppentheorie}{23}{section.3}
\contentsline {subsection}{\numberline {3.1}Theorie}{23}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Aufgaben}{29}{subsection.3.2}
\contentsline {section}{\numberline {4}Sylows\IeC {\"a}tze}{35}{section.4}
\contentsline {subsection}{\numberline {4.1}Theorie}{35}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Aufgaben}{38}{subsection.4.2}
